package com.mammoth.api.data.exceptions

class NoRecordException(customMessage: String? = null) : Exception(customMessage ?: "No Record Found!")
