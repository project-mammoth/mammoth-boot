package com.mammoth.api.data.repositories

data class DeleteResponse(val recordsDeleted: Number)