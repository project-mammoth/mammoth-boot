package com.mammoth.api.data.repositories

import com.mammoth.api.data.entities.Budget
import org.neo4j.driver.Record
import org.neo4j.springframework.data.repository.ReactiveNeo4jRepository
import org.neo4j.springframework.data.repository.query.Query
import reactor.core.publisher.Mono

interface BudgetRepository : ReactiveNeo4jRepository<Budget, String> {
    @Query("""
        MATCH (budget:Budget { id: ${'$'}id })
        DETACH DELETE budget
        RETURN count(budget) as recordsDeleted
    """)
    fun deleteBudgetById(id: String): Mono<Record>
}