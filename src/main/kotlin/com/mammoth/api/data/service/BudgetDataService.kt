package com.mammoth.api.data.service

import com.mammoth.api.dao.budget.BudgetDao
import com.mammoth.api.dao.budget.BudgetQueryDao
import com.mammoth.api.data.exceptions.NoRecordException
import com.mammoth.api.data.implementations.IBudgetDataService
import com.mammoth.api.data.repositories.BudgetRepository
import com.mammoth.api.dto.budget.BudgetDto
import com.mammoth.api.dto.common.DeleteResponseDto
import org.springframework.stereotype.Service


@Service
class BudgetDataService(private val budgetRepository: BudgetRepository) : IBudgetDataService {

    @Throws(NoRecordException::class)
    override fun getBudget(budgetId: String): BudgetDto {
        return budgetRepository.findById(budgetId).block()?.toDto() ?: throw NoRecordException()
    }

    override fun getBudgets(): List<BudgetDto> {
        return budgetRepository.findAll().map { it -> it.toDto() }.collectList().block()?.toList()
                ?: throw NoRecordException()
    }

    override fun deleteBudget(budgetId: String): DeleteResponseDto {
        val result = budgetRepository.deleteBudgetById("what the hell").block()
        print("response below")
        println(result)
        return DeleteResponseDto("waffles ", false, budgetId)
    }

    override fun updateBudget(budgetDao: BudgetDao): BudgetDto {
        TODO("Not yet implemented")
    }

    override fun createBudget(budgetDao: BudgetDao): BudgetDto {
        TODO("Not yet implemented")
    }

    override fun queryBudgets(budgetQueryDao: BudgetQueryDao): List<BudgetDto> {
        TODO("Not yet implemented")
    }
}