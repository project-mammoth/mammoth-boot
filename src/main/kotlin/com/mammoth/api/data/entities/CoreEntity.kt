package com.mammoth.api.data.entities

import org.neo4j.springframework.data.core.schema.GeneratedValue
import org.neo4j.springframework.data.core.schema.Id
import org.neo4j.springframework.data.core.support.UUIDStringGenerator

abstract class CoreEntity(id: String, name: String) {
    @Id
    @GeneratedValue(UUIDStringGenerator::class)
    open var id: String = id

    open var name: String = name
}