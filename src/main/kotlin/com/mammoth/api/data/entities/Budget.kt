package com.mammoth.api.data.entities

import com.mammoth.api.dto.budget.BudgetDto
import org.neo4j.springframework.data.core.schema.Node
import org.springframework.data.annotation.CreatedDate
import java.util.*


@Node(primaryLabel = "Budget")
class Budget(id: String, name: String) : CoreEntity(id, name) {
    @CreatedDate
    lateinit var createdDate: Date

    fun toDto(): BudgetDto {
        return BudgetDto(name, id, createdDate)
    }
}