package com.mammoth.api.data.implementations

import com.mammoth.api.dao.budget.BudgetDao
import com.mammoth.api.dao.budget.BudgetQueryDao
import com.mammoth.api.dto.budget.BudgetDto
import com.mammoth.api.dto.common.DeleteResponseDto

interface IBudgetDataService {
    fun getBudget(budgetId: String): BudgetDto
    fun getBudgets(): List<BudgetDto>
    fun deleteBudget(budgetId: String): DeleteResponseDto
    fun updateBudget(budgetDao: BudgetDao): BudgetDto
    fun createBudget(budgetDao: BudgetDao): BudgetDto
    fun queryBudgets(budgetQueryDao: BudgetQueryDao): List<BudgetDto>
}