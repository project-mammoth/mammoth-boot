package com.mammoth.api.business.implementations

import com.mammoth.api.dao.budget.BudgetDao
import com.mammoth.api.dao.budget.BudgetQueryDao
import com.mammoth.api.dto.budget.BudgetDto
import com.mammoth.api.dto.common.DeleteResponseDto

interface IBudgetService {
    fun createBudget(budgetDao: BudgetDao): BudgetDto
    fun queryBudget(budgetQueryDao: BudgetQueryDao): List<BudgetDto>
    fun getBudget(budgetId: String): BudgetDto
    fun updateBudget(budgetDao: BudgetDao): BudgetDto
    fun deleteBudget(budgetId: String): DeleteResponseDto
}