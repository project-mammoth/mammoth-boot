package com.mammoth.api.business.service

import com.mammoth.api.business.implementations.IBudgetService
import com.mammoth.api.dao.budget.BudgetDao
import com.mammoth.api.dao.budget.BudgetQueryDao
import com.mammoth.api.data.service.BudgetDataService
import com.mammoth.api.dto.budget.BudgetDto
import com.mammoth.api.dto.common.DeleteResponseDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class BudgetBusinessService : IBudgetService {

    @Autowired
    lateinit var budgetDataService: BudgetDataService

    override fun createBudget(budgetDao: BudgetDao) = budgetDataService.createBudget(budgetDao)

    override fun queryBudget(budgetQueryDao: BudgetQueryDao) = budgetDataService.queryBudgets(budgetQueryDao)

    override fun getBudget(budgetId: String): BudgetDto {
        return budgetDataService.getBudget(budgetId)
    }

    override fun updateBudget(budgetDao: BudgetDao): BudgetDto {
        return budgetDataService.updateBudget(budgetDao)
    }

    override fun deleteBudget(budgetId: String): DeleteResponseDto {
        return budgetDataService.deleteBudget(budgetId)
    }
}