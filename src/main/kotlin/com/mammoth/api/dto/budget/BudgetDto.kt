package com.mammoth.api.dto.budget

import java.util.*

data class BudgetDto(val name: String, val id: String, val createdDate: Date)