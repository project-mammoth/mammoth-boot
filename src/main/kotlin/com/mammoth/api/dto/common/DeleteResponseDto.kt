package com.mammoth.api.dto.common

data class DeleteResponseDto(val message: String, val isDeleted: Boolean, val id: String)