package com.mammoth.api.dto.account

import com.mammoth.api.constants.AccountType
import com.mammoth.api.dto.common.CoreNode

class AccountDto(private val id: String,
                 private val budgetId: String,
                 private val name: String,
                 private val type: AccountType,
                 private val balance: Number
) : CoreNode(id = id, name = name, budgetId = budgetId) {

}