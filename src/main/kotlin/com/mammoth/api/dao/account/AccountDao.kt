package com.mammoth.api.dao.account

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.mammoth.api.constants.AccountType
import lombok.Getter
import lombok.NoArgsConstructor
import lombok.Setter
import lombok.experimental.Accessors
import javax.validation.constraints.NotEmpty


/**
 * Created by Arpit Khandelwal.
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
class AccountDao {
    @NotEmpty(message = "{constraints.NotEmpty.message")
    private val budgetId: String? = null;

    @NotEmpty(message = "{constraints.NotEmpty.message")
    private val name: String? = null;

    @NotEmpty(message = "{constraints.NotEmpty.message")
//    TODO: @IsEnum(SupportedAccountType, {
//        message: 'Value must match a supported enum value',
//    })
    private val type: AccountType? = null;

    @NotEmpty(message = "{constraints.NotEmpty.message")
    // TODO: Is Number
    private val balance: Number? = null;
}