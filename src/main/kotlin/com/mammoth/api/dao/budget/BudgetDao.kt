package com.mammoth.api.dao.budget

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty


class BudgetDao @JsonCreator internal constructor(@param:JsonProperty("name") val name: String)
