package com.mammoth.api.controller

import com.mammoth.api.constants.AccountType
import com.mammoth.api.dao.account.AccountDao
import com.mammoth.api.dto.account.AccountDto
import com.mammoth.api.dto.common.DeleteResponseDto
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/v1/accounts")
class AccountController {

    @PostMapping("")
    fun createAccount(@RequestBody accountDao: AccountDao): AccountDto {
        return AccountDto("test",
                "test-budget",
                "createAccount",
                AccountType.Cash,
                100.99)
    }

    @GetMapping("{budgetId}")
    fun getAllAccountsForBudgetId(
            @PathVariable("budgetId") budgetId: String
    ): List<AccountDto> {
        return listOf()
        // TODO: Fill this in.
    }

    @GetMapping("{budgetId}/detail/{accountId}")
    fun getAccount(
            @PathVariable("budgetId") budgetId: String,
            @PathVariable("accountId") accountId: String
    ): AccountDto {
        return AccountDto("test",
                "test-budget",
                "getAccount",
                AccountType.Cash,
                100.99)
    }

    @PostMapping("{budgetId}/detail/{accountId}")
    fun updateAccountDetails(
            @PathVariable("budgetId") budgetId: String,
            @PathVariable("accountId") accountId: String,
            @RequestBody accountDao: AccountDao): AccountDto {
        return AccountDto("test",
                "test-budget",
                "updateAccountDetails",
                AccountType.Cash,
                100.99
        )
    }

    @DeleteMapping("{budgetId}/detail/{accountId}")
    fun deleteAccount(
            @PathVariable("budgetId") budgetId: String,
            @PathVariable("accountId") accountId: String
    ): DeleteResponseDto {
        return DeleteResponseDto("Deleted", true, "deleteAccount")
    }


}