package com.mammoth.api.controller

import com.mammoth.api.business.service.BudgetBusinessService
import com.mammoth.api.dao.budget.BudgetDao
import com.mammoth.api.dao.budget.BudgetQueryDao
import com.mammoth.api.dao.budget.BudgetUpdateDao
import com.mammoth.api.dto.budget.BudgetDto
import com.mammoth.api.dto.common.DeleteResponseDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*


@RestController
@RequestMapping("api/v1/budget")
class BudgetController {

    @Autowired
    lateinit var budgetService: BudgetBusinessService


    @PostMapping("")
    fun create(@RequestBody budgetDao: BudgetDao): BudgetDto {
        return budgetService.createBudget((budgetDao))
    }

    @GetMapping("")
    fun queryBudget(@RequestBody query: BudgetQueryDao): List<BudgetDto> {
        return listOf()
    }

    @GetMapping("{id}")
    fun getBudget(@PathVariable("id") id: String): BudgetDto {
        return budgetService.getBudget(id)
    }

    @PostMapping("{id}")
    fun updateExistingBudget(@PathVariable("id") id: String,
                             @RequestBody budget: BudgetUpdateDao): BudgetDto {
        return BudgetDto("", id, Date())
    }

    @DeleteMapping("{id}")
    fun deleteBudget(@PathVariable("id") id: String): DeleteResponseDto {
        return this.budgetService.deleteBudget(id)
    }
}