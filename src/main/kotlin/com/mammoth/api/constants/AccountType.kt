package com.mammoth.api.constants

enum class AccountType {
    Checking,
    Savings,
    Cash,
    CreditCard,
    LineOfCredit,
    Asset, // Investments, etc.
    Liability, // Mortgage, etc.
}