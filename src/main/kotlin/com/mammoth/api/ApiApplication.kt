package com.mammoth.api

import org.neo4j.springframework.data.repository.config.EnableNeo4jRepositories
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableNeo4jRepositories("com.mammoth.api.data.repositories")
class ApiApplication

fun main(args: Array<String>) {
	runApplication<ApiApplication>(*args)
}
